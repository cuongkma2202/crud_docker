import express from "express";
import morgan from "morgan";
import handlebars from "express-handlebars";
import path from "path";
import route from "./routes/index.js";
import connect from "./config/db/index.js";
const app = express();
const port = 3000;
const __dirname = path.resolve();
// http logger
app.use(morgan("dev"));
// static file
app.use(express.static(path.join(__dirname, "src", "public")));
// template engine
app.use(
    express.urlencoded({
        extended: true,
    })
);
// connect to DB
connect();
app.use(express.json());
app.engine(
    "hbs",
    handlebars.engine({
        extname: ".hbs",
    })
);
app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "src", "resources", "views"));
route(app);
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});