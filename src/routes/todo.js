import express from "express";
import Todo from "../app/controllers/TodoController.js";
const router = express.Router();

const todoController = new Todo();
router.post("/store", todoController.store);
router.get("/", todoController.show);

export default router;