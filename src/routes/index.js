import todoRoute from "./todo.js";
const route = (app) => {
    app.use("/", todoRoute);
};
export default route;