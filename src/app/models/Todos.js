import mongoose from "mongoose";
const Schema = mongoose.Schema;

const TodosSchema = new Schema({
    content: { type: String },
});
const Todo = mongoose.model("Todo", TodosSchema);
export default Todo;