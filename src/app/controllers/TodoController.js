import TodoModel from "../models/Todos.js";
class Todo {
    show(req, res, next) {
        TodoModel.find({})
            .then((todos) => {
                todos = todos.map((todo) => todo.toObject());
                console.log(todos);
                res.render("home", { todos });
            })
            .catch((err) => console.log(err));
    }
    store(req, res, next) {
        const todo = new TodoModel(req.body);
        todo
            .save()
            .then(() => res.redirect("/"))
            .catch((err) => console.log(err));
    }
}
export default Todo;